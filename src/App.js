import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

import D3 from './components/D3';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: {}
    };
  }


  handleFileUpload = (event) => {
    const file = event.target.files[0];
    if (!file) return;
    const data = new FormData();
    data.append('configFile', file);
    axios.post('/api/convertFile', data).then( response => this.setState({data: response.data.data}) )
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Zoomable Circle Packing</h1>
        </header>

        <label className="App-intro">
          Select the file to render SVG.
          <input type="file" onChange={this.handleFileUpload} />
        </label>

        {!!Object.keys(this.state.data).length && <D3 data={this.state.data} width={500} height={500}/>}
      </div>
    );
  }
}

export default App;
